# Mooc-frontend

We create nextjs with typescript application that describe about "Registration flow".

## Team members
- Trinh Van Dat
- Dang Tuan Nghia
- Vu Thanh Tam
- Bui Hoang Giang

## Description
We create the project about registration flow application to describe everything we learned about Frontend programming.

## Installation
Please follow step-by-step to installation our project:
1. Clone project by using git command: git clone https://gitlab.com/trinhvandat/mooc-frontend.git
2. In to project (with path: ~/mooc-frontend), using command "npm run dev" to run project.

## Teach stack
